<?php

declare(strict_types=1);

namespace App\Controller;


use App\Repository\BlogPostRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CustomerSiteController
 * @package App\Controller
 *
 * @Route(path="/blogpost")
 */
class BlogPostController
{
    private $blogPostRepository;

    public function __construct(BlogPostRepository $blogPostRepository)
    {
        $this->blogPostRepository = $blogPostRepository;
    }

    /**
     * @Route("/add", name="add_customer", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function addBlogPost(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $author = $data['author'];
        $email = $data['email'];
        $title = $data['title'];
        $shortBody = $data['shortBody'];
        $body = $data['body'];

        if (empty($author) || empty($email) || empty($title) || empty($shortBody) || empty($body)) {
            throw new NotFoundHttpException('Expect mandatory parameters!');
        }

        $this->blogPostRepository->saveBlogPost($author, $email, $title, $shortBody, $body);

        return new JsonResponse(['status' => 'Customer added!'], Response::HTTP_CREATED);

    }

    /**
     * @Route ("/posts/{id}", name="get_one_post",methods={"GET"})
     */
   public function getBlogPost($id):JsonResponse
   {
       $posts=$this->blogPostRepository->findOneBy(["id"=>$id]);
       $data=[
           'id'=>$posts->getId(),
            'author'=>$posts->getAuthor(),
            'email'=>$posts->getEmail(),
            'title'=>$posts->getTitle(),
            'shortBody'=>$posts->getShortBody(),
            'body'=>$posts->getBody()
       ];
       return new JsonResponse($data,Response::HTTP_OK);
   }
    /**
     * @Route("/get-all", name="get_all_post", methods={"GET"})
     */
    public function getAllCustomers(): JsonResponse
    {
        $posts = $this->blogPostRepository->findAll();
        $data = [];

        foreach ($posts as $post) {
            $data[] = [
                'id' => $post->getId(),
                'author'=>$post->getAuthor(),
                'email'=>$post->getEmail(),
                'title'=>$post->getTitle(),
                'shortBody'=>$post->getShortBody(),
                'body'=>$post->getBody()
            ];
        }

        return new JsonResponse(['posts' => $data], Response::HTTP_OK);
    }

    /**
     * @Route("/update/{id}", name="update_post", methods={"PUT"})
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function updateCustomer($id, Request $request): JsonResponse
    {
        $customer = $this->blogPostRepository->findOneBy(['id' => $id]);
        $data = json_decode($request->getContent(), true);

        $this->blogPostRepository->updateCustomer($customer, $data);

        return new JsonResponse(['status' => 'customer updated!']);
    }
    /**
     * @Route("/delete/{id}", name="delete_customer", methods={"DELETE"})
     */
    public function deleteCustomer($id): JsonResponse
    {
        $post = $this->blogPostRepository->findOneBy(['id' => $id]);

        $this->blogPostRepository->removeCustomer($post);

        return new JsonResponse(['status' => 'customer deleted']);
    }
}