<?php
declare(strict_types=1);
namespace App\Repository;

use App\Entity\BlogPost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BlogPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlogPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlogPost[]    findAll()
 * @method BlogPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogPostRepository extends ServiceEntityRepository
{
    public $manager;

    public function __construct
    (
        ManagerRegistry $registry,
        EntityManagerInterface $manager
    )
    {
        parent::__construct($registry, BlogPost::class);
        $this->manager = $manager;
    }

    public function saveBlogPost($author, $email, $title, $shortBody, $body)
    {
        $newBlogPost= new BlogPost();

        $newBlogPost
            ->setAuthor($author)
            ->setEmail($email)
            ->setTitle($title)
            ->setShortBody($shortBody)
            ->setBody($body);

        $this->manager->persist($newBlogPost);
        $this->manager->flush();
    }

    public function updateCustomer(BlogPost $post, $data)
    {
        empty($data['author']) ? true : $post->setAuthor($data['author']);
        empty($data['email']) ? true : $post->setEmail($data['email']);
        empty($data['title']) ? true : $post->setTitle($data['title']);
        empty($data['shortBody']) ? true : $post->setShortBody($data['shortBody']);
        empty($data['body']) ? true : $post->setBody($data['body']);

        $this->manager->flush();
    }

    public function removeCustomer(BlogPost $post)
    {
        $this->manager->remove($post);
        $this->manager->flush();
    }
    // /**
    //  * @return BlogPost[] Returns an array of BlogPost objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BlogPost
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
